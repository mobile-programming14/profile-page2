import 'package:flutter/material.dart';

enum APP_THEME { LIGHT, DARK }

void main() {
  runApp(ContactProfilePage());
}

class MyAppTheme {
  static ThemeData appThemeLight() {
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      iconTheme: IconThemeData(color: Colors.indigo.shade500),
    );
  }

  static ThemeData appThemeDark() {
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.white,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      iconTheme: IconThemeData(color: Colors.white),
    );
  }
}

class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyAppTheme.appThemeLight()
          : MyAppTheme.appThemeDark(),
      home: Scaffold(
        appBar: buildAppbarWidget(),
        body: buildBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.sunny),
          onPressed: () {
            setState(() {
              if(currentTheme == APP_THEME.DARK) {
                currentTheme = APP_THEME.LIGHT;
              }else {
                currentTheme = APP_THEME.DARK;
              }
            });
          },
        ),
      ),
    );
  }
}


Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
          color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}
Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          // color: Colors.cyanAccent,
        ),
        onPressed: () {},
      ),
      Text("Text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.cyanAccent,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}
Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.cyanAccent,
        ),
        onPressed: () {},
      ),
      Text("Directions"),
    ],
  );
}
Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          // color: Colors.cyanAccent,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}
Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          // color: Colors.cyanAccent,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

//  ListTile
Widget mobilePhoneListTile(){
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("097-012-8272"),
    subtitle: Text("mobile"),
    trailing: IconButton (
      icon: Icon(Icons.message),
      // color: Colors.cyanAccent,
      onPressed: () {},
    ),
  );
}
Widget otherPhoneListTile(){
  return ListTile(
    leading: Text(""),
    title: Text("440-440-3390"),
    subtitle: Text("other"),
    trailing: IconButton (
      icon: Icon(Icons.message),
      // color: Colors.cyanAccent,
      onPressed: () {},
    ),
  );
}
Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("63160284@go.buu.ac.th"),
    subtitle: Text("work"),
    trailing: Text(""),
  );
}
Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_on),
    title: Text("Rayong, Thailand"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      // color: Colors.cyanAccent,
      onPressed: () {},
    ),
  );
}
AppBar buildAppbarWidget(){
  return AppBar(
    backgroundColor: Colors.tealAccent,
    leading: Icon(Icons.arrow_back),
    actions: <Widget>[
      IconButton(onPressed: () {}, icon: Icon(Icons.star_border_outlined)),
    ],
  );
}

Widget buildBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 270,

            child: Image.network(
              "https://github.com/ptyagicodecamp/educative_flutter/raw/profile_1/assets/profile.jpg?raw=true",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "Thanya Akharawimonkun",
                    style: TextStyle(fontSize: 30),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
            thickness: 0.3,
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(
                  iconTheme: IconThemeData(
                    color: Colors.grey,
                  )),
              child: profileActionItems(),
            ),
          ),
          Divider(
            color: Colors.black,
            thickness: 0.7,
          ),
          mobilePhoneListTile(),
          // otherPhoneListTile(),
          Divider(
            color: Colors.black,
            thickness: 0.3,
          ),
          emailListTile(),
          addressListTile(),
        ],
      ),
    ],
  );
}

Widget profileActionItems() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton()
    ],
  );
}




